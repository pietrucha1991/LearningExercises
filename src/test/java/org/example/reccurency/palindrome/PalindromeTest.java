package org.example.reccurency.palindrome;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PalindromeTest {


    @Test
    void shouldTestPalindrome() {
        //given
        String abba = "ABBA";
        String budda = "BUDDA";
        //when
        boolean abbaTest = Palindrome.isPalindrome(abba);
        boolean buddaTest = Palindrome.isPalindrome(budda);
        //then
        assertTrue(abbaTest);
        assertFalse(buddaTest);
    }



}
