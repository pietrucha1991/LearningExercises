package org.example.reccurency.fibonacci;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class FibonacciTest {

    @Test
    void fibonacciPlainRecursion() {
        //given
        int numberToCheck = 3;
        //when
        int fibonacciResult = Fibonacci.fibonacciPlainRecursion(numberToCheck);
        //then
        assertEquals(fibonacciResult, 2);
    }
}
