package org.example.tasks.anagrams;

import org.example.tasks.TenBeginnerCodeChallenges.anagrams.Anagrams;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class AnagramsTest {

    @ParameterizedTest
    @MethodSource("prepareSourceSimple")
    void shouldTestAnagramsSimple(String firstWord, String secondWord, boolean expectedResult) {
        //given, when
        boolean result = Anagrams.isAnagram(firstWord, secondWord);

        //then
        assertEquals(result, expectedResult);
    }

    private static Stream<Arguments> prepareSourceSimple() {
        return Stream.of(
                Arguments.of("listen", "silent", true),
                Arguments.of("brainy", "binary", true),
                Arguments.of("Paris", "pairs", true)
        );
    }

    @Test
    void shouldTestGroupingAnagrams() {
        //given
        List<String> input = List.of("tar", "rat", "art", "meats", "steam");
        //when
        List<List<String>> resultNoSet = Anagrams.groupAnagramsNoSet(input);
        List<List<String>> resultSet = Anagrams.groupAnagramsUsingSet(input);
        //then
        assertThat(resultSet).containsExactlyInAnyOrder(List.of("meats", "steam"), List.of("tar", "rat", "art"));
        assertThat(resultNoSet).containsExactlyInAnyOrder(List.of("meats", "steam"), List.of("tar", "rat", "art"));
    }


}
