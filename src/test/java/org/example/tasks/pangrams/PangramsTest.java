package org.example.tasks.pangrams;

import org.example.tasks.TenBeginnerCodeChallenges.pangrams.Pangrams;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class PangramsTest {

    @Test
    void isPangram() {
        String input =" The quick brown fox jumps over the lazy dog.";
        boolean isPangram = Pangrams.isPangram(input);
        assertTrue(isPangram);
    }

    @Test
    void isPerfectPangram() {
        String input = "Mr. Jock, TV quiz Ph.D., bags few lynx";
        boolean isPerfectPangram = Pangrams.isPerfectPangram(input);
        assertFalse(isPerfectPangram);
    }

    @ParameterizedTest
    @MethodSource("prepareSource")
    void shouldTestisPerfectPangram(String input, boolean expectedBoolean){
        //given, when
        boolean isPerfectPangram = Pangrams.isPerfectPangram(input);

        //then
        assertEquals(isPerfectPangram, expectedBoolean);
    }

    static Stream<Arguments> prepareSource(){
        return Stream.of(
                Arguments.of("Two driven jocks help fax my big quiz.", false),
                Arguments.of("Mr. Jock, TV quiz Ph.D., bags few lynx", true),
                Arguments.of("Crwth vox zaps qi gym fjeld bunk.", true),
                Arguments.of("Squdgy kilp job zarf nth cwm vex.", true)
        );
    }
}
