package org.example.tasks.findTheWord;

import org.example.tasks.TenBeginnerCodeChallenges.findTheWord.FindTheWord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FindTheWordTest {

    @ParameterizedTest
    @MethodSource("prepareSource")
    void shouldTestFindTheWord(String input, int wordPosition, String expectedResult) {
        //given, when
        String result = FindTheWord.findWordAtPosition(input, wordPosition);

        //then
        assertEquals(result,expectedResult);
    }

    static Stream<Arguments> prepareSource() {
        return Stream.of(
                Arguments.of("I love Codecademy", 1, "I"),
                Arguments.of("I love Codecademy", 2, "love"),
                Arguments.of("I love Codecademy", 3, "Codecademy")
        );
    }

    @Test
    void shouldFindSecondToLastWord() {
        //given
        String input = "I love Codecademy";
        String expectedResult = "love";
        //when
        String result = FindTheWord.findSecondToLastWord(input);
        //then
        assertEquals(result, expectedResult);
    }
}
