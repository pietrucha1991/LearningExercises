package org.example.tasks;

import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MySimpleListTest {

    @Test
    void smokeTest() {
        //init
        final MySimpleList<Integer> list = new MySimpleList<>();
        assertThat(list.size()).isEqualTo(0);
        assertThatThrownBy(() -> list.get(0)).isInstanceOf(IndexOutOfBoundsException.class);
        assertThatThrownBy(() -> list.get(1)).isInstanceOf(IndexOutOfBoundsException.class);
        assertThatThrownBy(() -> list.get(2)).isInstanceOf(IndexOutOfBoundsException.class);

        //add some stuff
        final int first = 1;
        final int second = 5;
        final int third = 3;

        list.add(first);
        assertThat(list.size()).isEqualTo(1);
        assertThat(list.get(0)).isEqualTo(first);

        list.add(second);
        assertThat(list.size()).isEqualTo(2);
        assertThat(list.get(0)).isEqualTo(first);
        assertThat(list.get(1)).isEqualTo(second);

        list.add(third);
        assertThat(list.size()).isEqualTo(3);
        assertThat(list.get(0)).isEqualTo(first);
        assertThat(list.get(1)).isEqualTo(second);
        assertThat(list.get(2)).isEqualTo(third);

        //remove from the end
        list.remove(2);
        assertThat(list.size()).isEqualTo(2);
        assertThat(list.get(0)).isEqualTo(first);
        assertThat(list.get(1)).isEqualTo(second);
        assertThatThrownBy(() -> list.get(2)).isInstanceOf(IndexOutOfBoundsException.class);

        list.remove(1);
        assertThat(list.size()).isEqualTo(1);
        assertThat(list.get(0)).isEqualTo(first);
        assertThatThrownBy(() -> list.get(1)).isInstanceOf(IndexOutOfBoundsException.class);
        assertThatThrownBy(() -> list.get(2)).isInstanceOf(IndexOutOfBoundsException.class);

        list.remove(0);
        assertThat(list.size()).isEqualTo(0);
        assertThatThrownBy(() -> list.get(0)).isInstanceOf(IndexOutOfBoundsException.class);
        assertThatThrownBy(() -> list.get(1)).isInstanceOf(IndexOutOfBoundsException.class);
        assertThatThrownBy(() -> list.get(2)).isInstanceOf(IndexOutOfBoundsException.class);

        //add some stuff again
        list.add(first);
        assertThat(list.size()).isEqualTo(1);
        assertThat(list.get(0)).isEqualTo(first);

        list.add(second);
        assertThat(list.size()).isEqualTo(2);
        assertThat(list.get(0)).isEqualTo(first);
        assertThat(list.get(1)).isEqualTo(second);

        list.add(third);
        assertThat(list.size()).isEqualTo(3);
        assertThat(list.get(0)).isEqualTo(first);
        assertThat(list.get(1)).isEqualTo(second);
        assertThat(list.get(2)).isEqualTo(third);

        //remove from the start
        list.remove(0);
        assertThat(list.size()).isEqualTo(2);
        assertThat(list.get(0)).isEqualTo(second);
        assertThat(list.get(1)).isEqualTo(third);
        assertThatThrownBy(() -> list.get(2)).isInstanceOf(IndexOutOfBoundsException.class);

        list.remove(0);
        assertThat(list.size()).isEqualTo(1);
        assertThat(list.get(0)).isEqualTo(third);
        assertThatThrownBy(() -> list.get(1)).isInstanceOf(IndexOutOfBoundsException.class);
        assertThatThrownBy(() -> list.get(2)).isInstanceOf(IndexOutOfBoundsException.class);

        list.remove(0);
        assertThat(list.size()).isEqualTo(0);
        assertThatThrownBy(() -> list.get(0)).isInstanceOf(IndexOutOfBoundsException.class);
        assertThatThrownBy(() -> list.get(1)).isInstanceOf(IndexOutOfBoundsException.class);
        assertThatThrownBy(() -> list.get(2)).isInstanceOf(IndexOutOfBoundsException.class);

        //test adding tons of elemets
        IntStream.rangeClosed(1, 100)
                .boxed()
                .forEach(list::add);

        assertThat(list.size()).isEqualTo(100);

        IntStream.rangeClosed(0, 99)
                .boxed()
                .forEach(index -> {
                    final var expectedNumber = index + 1;
                    assertThat(list.get(index)).isEqualTo(expectedNumber);
                });
    }


    @Test
    void shouldNotHaveAnyEmptySpaces() {
        final MySimpleList<String> list = new MySimpleList<>();

        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");
        list.add("f");
        list.add("g");
        list.add("h");
        list.add("i");
        list.add("j");

        assertThat(list.size()).isEqualTo(10);

        list.remove(9);
        assertThat(list.size()).isEqualTo(9);
        assertThatThrownBy(() -> list.get(9)).isInstanceOf(IndexOutOfBoundsException.class);
    }
}
