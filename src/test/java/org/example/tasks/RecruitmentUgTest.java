package org.example.tasks;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class RecruitmentUgTest {

    static final int[] COLUMN_1 = {1, 2, 5, 7};
    static final int[] COLUMN_2 = {12, 21, 50, 7};
    static final int[] COLUMN_3 = {14, 6, 5, 78};

    static final int[][] ARRAY_2D_1 = {COLUMN_1, COLUMN_2, COLUMN_3};
    static final int[][] ARRAY_2D_2 = {COLUMN_3, COLUMN_1, COLUMN_2};

    final RecruitmentUg classUnderTest = new RecruitmentUg();

    @ParameterizedTest
    @MethodSource("provide2DArrays")
    void shouldFindArray(final int[][] array, final int[] subArray, final int expectedIndex) {
        //given, when
        final int result = classUnderTest.findSubArray(array, subArray);

        //then
        assertThat(result).isEqualTo(expectedIndex);
    }

    public static Stream<Arguments> provide2DArrays() {
        return Stream.of(
                Arguments.of(ARRAY_2D_1, COLUMN_1, 0),
                Arguments.of(ARRAY_2D_1, COLUMN_2, 1),
                Arguments.of(ARRAY_2D_1, COLUMN_3, 2),

                Arguments.of(ARRAY_2D_2, COLUMN_1, 1),
                Arguments.of(ARRAY_2D_2, COLUMN_2, 2),
                Arguments.of(ARRAY_2D_2, COLUMN_3, 0),

                Arguments.of(ARRAY_2D_1, new int[]{14, 6, 5, 78, 5}, -1),
                Arguments.of(ARRAY_2D_1, new int[]{1, 3, 5, 7}, -1),
                Arguments.of(ARRAY_2D_1, new int[]{1, 2, 6, 7}, -1),
                Arguments.of(ARRAY_2D_1, new int[]{1, 2, 5, 8}, -1),
                Arguments.of(ARRAY_2D_1, new int[]{1, 2, 5, 8, 12}, -1),
                Arguments.of(ARRAY_2D_1, new int[]{1, 2, 5}, -1),

                Arguments.of(new int[][]{}, new int[]{}, -1),
                Arguments.of(new int[][]{new int[]{1,2,3}, new int[]{1}}, new int[]{1}, 1),
                Arguments.of(new int[][]{COLUMN_1, new int[]{}, COLUMN_3}, new int[]{}, 1)
        );
    }

    @ParameterizedTest
    @MethodSource("provideArrays")
    void shouldFindArray(final int[] subArray, final int expectedIndex) {
        //given
        final int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 1, 5, 6};
        // when
        final int result = classUnderTest.findSubArray(array, subArray);

        //then
        assertThat(result).isEqualTo(expectedIndex);
    }

    public static Stream<Arguments> provideArrays() {
        return Stream.of(
                Arguments.of(new int[]{1, 2}, 1),
                Arguments.of(new int[]{1, 2, 5}, -1),
                Arguments.of(new int[]{5, 6, 7, 8, 9}, -1),
                Arguments.of(new int[]{0, 2}, -1),
                Arguments.of(new int[]{}, -1),
                Arguments.of(new int[]{0, 1}, 0),
                Arguments.of(new int[]{1, 5, 6}, 9),
                Arguments.of(new int[]{1, 4, 8}, -1),
                Arguments.of(new int[]{4, 5, 6}, 4),
                Arguments.of(new int[]{7, 8, 1}, 7)
        );
    }

    @Test
    void shouldParseUrlParams() {
        //given
        final String url = "www.some.url/car/search?location=warsaw&color=black&make=jeep&model=liberty";

        //when
        final Map<String, String> result = classUnderTest.getUrlParameters(url);

        //then
        assertThat(result).hasSize(4);
        assertThat(result).containsEntry("location", "warsaw");
        assertThat(result).containsEntry("color", "black");
        assertThat(result).containsEntry("make", "jeep");
        assertThat(result).containsEntry("model", "liberty");
    }

    @Test
    void shouldReturnEmptyResultIfNoParamsPresent() {
        //given
        final String url = "www.some.url/car/search";

        //when
        final Map<String, String> result = classUnderTest.getUrlParameters(url);

        //then
        assertThat(result).isEmpty();
    }

    @Test
    void shouldHandleMalfunctionedUrls() {
        //given
        final String url = "www.some.url/car/search?test?error?omg";

        //when
        final Map<String, String> result = classUnderTest.getUrlParameters(url);

        //then
        assertThat(result).isEmpty();
    }

    @ParameterizedTest
    @MethodSource("provideLoopyDeepEqualsTestData")
    void shouldCheckIfTwoArraysAreEqual(final int[] first, final int[] second, final boolean expected) {
        //given, when
        final boolean result = classUnderTest.loopyDeepEquals(first, second);
        //then
        assertThat(result).isEqualTo(expected);
    }

    public static Stream<Arguments> provideLoopyDeepEqualsTestData() {
        return Stream.of(
                Arguments.of(new int[]{1, 2}, new int[]{1, 2}, true),
                Arguments.of(new int[]{1, 2}, new int[]{1, 2, 3}, false),
                Arguments.of(new int[]{1, 2, 3}, new int[]{1, 2}, false),
                Arguments.of(new int[]{1, 2, 3}, new int[]{1, 2, 3}, true),
                Arguments.of(new int[]{}, new int[]{1, 2}, false),
                Arguments.of(new int[]{1, 2}, new int[]{}, false)
        );
    }
}
