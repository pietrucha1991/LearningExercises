package org.example.tasks.primeNumberChecker;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PrimeNumberCheckerTest {

    @ParameterizedTest
    @ValueSource(ints = {2, 3, 7, 11, 13, 19})
    void shouldTestPrimeNumberCheckerAsTrue(int input) {
        //given, when
        boolean isPrime = PrimeNumberChecker.isPrimeNumber(input);
        //then
        assertTrue(isPrime);
    }

    @ParameterizedTest
    @ValueSource(ints = {4, 6, 8, 10, 12})
    void shouldTestPrimeNumberCheckerAsFalse(int input) {
        //given, when
        boolean isPrime = PrimeNumberChecker.isPrimeNumber(input);
        //then
        assertFalse(isPrime);
    }

    @ParameterizedTest
    @MethodSource("prepareSource")
    void shouldGetListOfPrimeNumbers(int input, List<Integer> expectedResult) {
        //given, when
        List<Integer> result = PrimeNumberChecker.getPrimeNumbers(input);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(expectedResult);
    }

    static Stream<Arguments> prepareSource() {
        return Stream.of(
                Arguments.of(10, List.of(2, 3, 5, 7)),
                Arguments.of(20, List.of(2, 3, 5, 7, 11, 13, 17, 19))
        );
    }

}
