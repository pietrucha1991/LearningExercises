package org.example.tasks.TenBeginnerCodeChallenges;

import org.example.tasks.TenBeginnerCodeChallenges.wordReversal.WordReversal;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class WordReversalTest {


    @ParameterizedTest
    @MethodSource("prepareSource")
    void shouldTestReversingWord(String toTest, String expectedResult) {
        //given, when
        String result = WordReversal.reverse(toTest);
        //then
        assertEquals(result, expectedResult);
    }

    static Stream<Arguments> prepareSource(){
        return Stream.of(
                Arguments.of("Dog bites man", "Man bites dog"),
                Arguments.of("Codecademy is the best!", "Best the is codecademy!")
        );
    }

}
