package org.example.tasks.wordSearch;

import org.example.tasks.TenBeginnerCodeChallenges.wordSearch.WordSearch;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WordSearchTest {
    @Test
    void shouldTestIfStartsWith() {
        //given
        String toTest = "hello world";
        String toFind = "hello";
        boolean expectedResult = true;
        //when
        boolean result = WordSearch.startsWith(toTest, toFind);
        //then
        assertEquals(result, expectedResult);
    }
    @Test
    void shouldTestCountingOccurrences() {
        //given
        String toTest = "I’m the new newt,";
        String toFind = "new";
        int expectedResult = 2;
        //when
        int result = WordSearch.countOccurrences(toTest, toFind);
        //then
        assertEquals(result, expectedResult);
    }

}
