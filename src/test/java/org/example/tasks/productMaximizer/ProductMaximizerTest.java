package org.example.tasks.productMaximizer;

import org.example.tasks.TenBeginnerCodeChallenges.productMaximizer.ProductMaximizer;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ProductMaximizerTest {

    @Test
    void shouldTestProductMaximizer() {
        //given
        int[] input = {1, 3, 61, 1, 5};
        int[] expectedResult = {61, 5, 305};

        //when
        int[] result = ProductMaximizer.ProductMaximizer(input);

        //then
        assertTrue(Arrays.equals(expectedResult, result));
    }

    @Test
    void shouldTestProductMaximizerWithTwoArrays() {
        //then
        int[] inputOne = {1, 3, 61, 1, 5};
        int[] inputTwo = {4, 2, 1, 7, 8, 3, 21};
        int[] expectedResult = {61, 21, 1281};

        //when
        int[] result = ProductMaximizer.ProductMaximizer(inputOne, inputTwo);

        //then
        assertTrue(Arrays.equals(result, expectedResult));
    }
}
