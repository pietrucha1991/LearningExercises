package org.example.tasks.balanced;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MaciekTest {

    final BalancedWords maciek = new Maciek();

    @ParameterizedTest
    @MethodSource("provideGoodScenarios")
    void shouldCount(final String input, final int expected) {
        // given, when
        final int result = maciek.countBalancedWords(input);
        //then
        assertEquals(expected, result);
    }

    static Stream<Arguments> provideGoodScenarios() {
        return Stream.of(
                Arguments.of("aabbabcccba", 28),
                Arguments.of("", 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideBadScenarios")
    void shouldThrowExcpetionIfInputIsIncorrect(final String input) {
        // given, when, then
        assertThrows(RuntimeException.class, () -> maciek.countBalancedWords(input));
    }

    static Stream<Arguments> provideBadScenarios() {
        return Stream.of(
                Arguments.of("abababa1"),
                Arguments.of((Object) null)
        );
    }

}