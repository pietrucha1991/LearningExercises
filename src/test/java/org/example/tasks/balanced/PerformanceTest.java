package org.example.tasks.balanced;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PerformanceTest {

    final BalancedWords leszek = new Leszek();
    final BalancedWords maciek2 = new Maciek2();

    final BalancedWords lukasz = new Lukasz();
    final List<BalancedWords> allImplementations = List.of(leszek, maciek2, lukasz);
    final StopWatch watch = new StopWatch();

    @BeforeEach
    void init() {
        watch.reset();
    }

    @ParameterizedTest
    @MethodSource("provideAlphabet")
    void alphabet(final String input) {
        allImplementations.forEach(impl -> measurePerdormance(input, impl));
    }

    public static Stream<String> provideAlphabet() {
        return Stream.of(1, 4, 30)
                .map(length -> createString("abcdefghijklmnoprstuwz", length));
    }

    @ParameterizedTest
    @MethodSource("provideA")
    void all(final String input) {
        allImplementations.forEach(impl -> measurePerdormance(input, impl));
    }

    public static Stream<String> provideA() {
        return Stream.of(1, 4, 100, 1_000 )
                .map(length -> createString("a", length));
    }

    public static String createString(String input, int length) {
        return IntStream.rangeClosed(1, length).boxed()
                .map(shift -> input)
                .collect(Collectors.joining());
    }

    private void measurePerdormance(final String input, final BalancedWords balancedWords) {
        watch.reset();
        System.out.println("\n=======================");
        watch.start();
        final int result = balancedWords.countBalancedWords(input);
        watch.stop();
        System.out.printf("Implementation: '%s'. Input's length: %s. Result: %s. Time %s ms",  balancedWords.name(), input.length(), result, watch.getTime());
    }

}
