package org.example.tasks.balanced;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

class LukaszTest {

    final Lukasz lukasz = new Lukasz();

    @Test
    void shouldTestCountingBalancedSubwords() {
        //given
        String toTest = "aabbabcccba";

        //when
        int amountOfBalancedWords = lukasz.countBalancedWords(toTest);

        //then
        assertEquals(amountOfBalancedWords, 28);
    }

    @Test
    void shouldReturnZeroIfInputIsEmpty() {
        //given
        String input = "";

        //when
        int amountOfBalancedWords = lukasz.countBalancedWords(input);

        //then
        assertEquals(amountOfBalancedWords, 0);
    }

    @Test
    void shouldThrowExceptionWhenInputIsNull(){
        //given
        String input = null;
        //when, then
        assertThatThrownBy(() -> lukasz.countBalancedWords(input)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void shouldThrowExceptionWhenInputContainsWrongChars(){
        //given
        String input = "abscas2";
        //when, then
        assertThatThrownBy(() -> lukasz.countBalancedWords(input)).isInstanceOf(RuntimeException.class);
    }
}
