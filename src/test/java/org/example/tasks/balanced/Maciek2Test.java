package org.example.tasks.balanced;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class Maciek2Test {

    final BalancedWords maciek = new Maciek2();

    @ParameterizedTest
    @MethodSource("provideGoodScenarios")
    void shouldCount(final String input, final int expected) {
        // given, when
        final int result = maciek.countBalancedWords(input);
        //then
        assertEquals(expected, result);
    }

    static Stream<Arguments> provideGoodScenarios() {
        return Stream.of(
                Arguments.of("aabbabcccba", 28),
                Arguments.of("", 0)
        );
    }
}