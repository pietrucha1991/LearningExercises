package org.example.tasks.armstrongNumber;

/*
 * An Armstrong number is a whole number that’s equal to the sum of its digits raised to the power of the total number of digits.
 * For example, 153 is an Armstrong number because there are three digits, and 153 = 13 + 53 + 33.
 * The four-digit number 8208 is also an Armstrong number, as 8208 = 84 + 24 + 04 + 84.
 * Create an Armstrong number checker that returns a Boolean TRUE if the input number is an Armstrong number.
 * Hint: to extract each digit from a given number, try using the remainder/modulo operator.

 * If you’re looking for something a little more challenging, create an Armstrong number calculator that returns
 * all Armstrong numbers between 0 and the input number.*/

import java.util.ArrayList;
import java.util.List;

public class ArmstrongNumber {

    static boolean isArmstrongNumber(int input) {
        List<Integer> numbers = new ArrayList<>();
        int tempInput = input;
        int supposedPower = 0;

        while (tempInput > 0) {
            int remainder = tempInput % 10;
            tempInput /= 10;
            numbers.add(remainder);
            supposedPower++;
        }

        int sum = 0;
        for (int i = 0; i < numbers.size(); i++) {
            int powerOfNumber = countPower(numbers.get(i), supposedPower);
            sum += powerOfNumber;
        }
        return sum == input;
    }

    //Yes, I know that there is a Math.pow method, but I wanted to come up with my own algorithm for counting power
    static int countPower(int n, int power) {
        int baseNumber = n;
        for (int i = 1; i < power; i++) {
            n *= baseNumber;
        }
        return n;
    }

    static List<Integer> getArmstrongNumbers(int input) {
        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i <= input; i++) {
            if (isArmstrongNumber(i)) {
                numbers.add(i);
            }
        }

        return numbers;
    }

}
