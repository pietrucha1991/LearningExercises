package org.example.tasks.armstrongNumber;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ArmstrongNumberTest {

    @Test
    void isArmstrongNumber() {
        //given
        int input = 5;

        //when
        boolean result = ArmstrongNumber.isArmstrongNumber(input);

        //then
        assertTrue(result);
    }

    @Test
    void shouldReturnListOfArmstrongNumbers() {
        //given
        int input = 370;
        List<Integer> expectedResult = List.of(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 153, 370);
        //when
        List<Integer> armstrongNumbers = ArmstrongNumber.getArmstrongNumbers(input);

        //then
        assertFalse(armstrongNumbers.isEmpty());
        assertEquals(expectedResult, armstrongNumbers);
    }
}
