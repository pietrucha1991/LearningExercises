package org.example.tasks;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class TextTasksTest {

    private static final String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris lacinia orci nec tortor vestibulum, ac hendrerit dui condimentum. Nunc quis porta orci. Donec fermentum bibendum mollis. Integer pellentesque fringilla erat quis mollis. Proin in odio gravida, ornare massa a, interdum tellus. Duis lectus arcu, imperdiet sed commodo at, hendrerit nec eros. Donec dictum rutrum lectus id lobortis. Aenean in sollicitudin sem.\n" +
            "\n" +
            "Duis ultrices libero et arcu fringilla, nec porttitor libero commodo. Duis vehicula dapibus commodo. Donec molestie, justo eu pellentesque dapibus, felis metus faucibus nulla, eget tempus est metus vitae massa. Phasellus at semper metus. Phasellus sed dolor tincidunt, tempor leo quis, elementum nisl. Aliquam sagittis, justo non tempor suscipit, nunc nibh mollis velit, quis condimentum mauris enim quis odio. Suspendisse commodo libero blandit, interdum nisi eu, convallis odio. Vestibulum pellentesque ullamcorper metus vitae ultrices. Nunc mattis sagittis odio, in vulputate mi iaculis non. Quisque vestibulum nunc dui, ut gravida orci porttitor convallis. Donec id elit id risus sagittis pulvinar ut id turpis. Cras placerat ac purus a mollis. Cras porta vestibulum dictum. Pellentesque facilisis in tortor non fermentum. Vivamus lacinia mauris sed aliquet consequat.\n" +
            "\n" +
            "Aenean a tellus sem. Etiam enim mi, facilisis vel dignissim vel, vestibulum sit amet lorem. Donec scelerisque malesuada leo, ut condimentum lorem viverra nec. Etiam gravida consequat eleifend. Maecenas interdum consectetur luctus. Sed ipsum ex, placerat ut euismod scelerisque, porttitor eget leo. Curabitur tincidunt malesuada diam, in interdum lectus molestie ac. Proin molestie luctus velit, eget finibus dolor dictum eget. Ut cursus purus vel malesuada vestibulum. In vestibulum aliquam lectus at porttitor.\n" +
            "\n" +
            "Ut et gravida massa. Maecenas enim ipsum, lobortis eu lobortis ut, tincidunt eget dui. Sed non magna vulputate libero faucibus laoreet sed sit amet urna. Nam augue elit, fermentum eu vehicula nec, rutrum sed erat. Nullam porttitor ac ante vel dapibus. Nam sed cursus tellus. Vivamus a condimentum lacus. Nullam sodales posuere dui nec pretium. Proin finibus, odio sed venenatis rhoncus, elit elit mollis purus, eu placerat mi erat eu arcu. Praesent in sodales lectus, in tempor lacus.\n" +
            "\n" +
            "Duis sed tempor nisi. Integer ornare enim at massa gravida consequat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent et lacus consequat ipsum pellentesque ullamcorper. Quisque rutrum tempor erat, ac scelerisque urna condimentum vel. Donec at nisi faucibus, fermentum eros nec, aliquam neque. Aenean ullamcorper bibendum leo, ut pharetra enim vestibulum et. Phasellus dignissim metus sed dui placerat consequat. Nam at venenatis velit, non porttitor justo.";


    final TextTasks textTasks = new TextTasks();

    @Test
    void shouldCountWords() {
        //given
        final String sentence = "This is a sentence. This is a nice sentence. Sentence sounds funny";

        //when
        final Map<String, Integer> result = textTasks.countWords(sentence);

        //then
        final Map<String, Integer> expected = Map.of(
                "this", 2,
                "is", 2,
                "a", 2,
                "nice", 1,
                "sentence", 3,
                "sounds", 1,
                "funny", 1
        );
        assertThat(result).containsExactlyInAnyOrderEntriesOf(expected);

    }

    @Test
    void shouldGetSummary() {
       //todo
    }

}
