package org.example.tasks.numberReversal;

import org.example.tasks.TenBeginnerCodeChallenges.numberReversal.NumberReversal;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberReversalTest {

    @Test
    void numberReversal() {
        //given
        int number = 456;

        //when
        int result = NumberReversal.numberReversal(number);

        //then
        assertEquals(result, 654);
    }

    @Test
    void shouldTestDecimalReversal() {
        //given
        double number = 193.56;

        //when
        double result = NumberReversal.decimalNumberReversal(number);


        //then
        assertEquals(result, 653.91);
    }
}
