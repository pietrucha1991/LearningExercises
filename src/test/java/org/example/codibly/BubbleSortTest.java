package org.example.codibly;

import org.example.codibly.bubbleSort.BubbleSort;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;

class BubbleSortTest {
    @Test
    void shouldSortIntegers() {
        //given
        List<Integer> integers = new ArrayList<>(List.of(1, 4, 5, 6, 8, 3, 8));
        List<Integer> sorted = List.of(1, 3, 4, 5, 6, 8, 8);
        //when
        List<Integer> result = BubbleSort.sort(integers);
        //then
        assertEquals(sorted, result);
    }

    @Test
    void shouldSortDoubles() {
        //given
        List<Double> doubles = new ArrayList<>(List.of(-9.3, 0.2, 4.0, 0.1, 5.0, 9.0));
        List<Double> sorted = List.of(-9.3, 0.1, 0.2, 4.0, 5.0, 9.0);

        //when
        List<Double> result = BubbleSort.sort(doubles);
        //then
        assertEquals(sorted, result);
    }

    @Test
    void shouldReturnEmptyListWhenInputIsEmpty() {
        //given
        List<Double> doubles = new ArrayList<>();
        //when
        List<Double> result = BubbleSort.sort(doubles);
        //then
        assertTrue(result.isEmpty());
    }

    @Test
    void shouldRemoveNullElementFromTheList() {
        //given
        List<Double> doubles = new ArrayList<>();
        doubles.add(null);
        doubles.add(5.00001);
        List<Double> resultExpected = List.of(5.00001);
        //when
        List<Double> result = BubbleSort.sort(doubles);
        //then
        assertEquals(resultExpected, result);
    }


    @Test
    void shouldThrowErrorWhenListIsNull() {
        //given
        List<Double> doubles = null;
        //when, then
        assertThatThrownBy(() -> BubbleSort.sort(doubles)).isInstanceOf(RuntimeException.class);
    }
}
