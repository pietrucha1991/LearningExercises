package org.example.arrays.extractingString;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

class ExtractingIntegersTest {

    final ExtractingIntegers extractingIntegers = new ExtractingIntegers();

    @ParameterizedTest
    @MethodSource("prepareSource")
    void extractingIntegers(String word, List<Integer> result) {
        extractingIntegers.extractingIntegers(word);
    }

    public static Stream<Arguments> prepareSource(){
        return Stream.of(
                Arguments.of("cv dd 4 k 2321 2 11 k4k2 66 4d", List.of(4,2321,2,11,4,2,66,4)),
                Arguments.of("cv dd 4 k 2321 2 11 k4k2 66 4d", List.of(4,2321,2,11,4,2,66,4))
        );
    }
}
