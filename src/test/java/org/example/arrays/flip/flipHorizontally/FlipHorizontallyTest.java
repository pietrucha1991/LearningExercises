package org.example.arrays.flip.flipHorizontally;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FlipHorizontallyTest {

        @Test
        void flipHorizontally()
        {
                final Integer[][] horiNumbers = { { 1, 2, 3 },
                        { 4, 5, 6 },
                        { 7, 8, 9 }};
                final Integer[][] expected = { { 3, 2, 1 },
                        { 6, 5, 4 },
                        { 9, 8, 7 } };
                FlipHorizontally.flipHorizontally(horiNumbers);
                assertArrayEquals(expected, horiNumbers);
        }
}
