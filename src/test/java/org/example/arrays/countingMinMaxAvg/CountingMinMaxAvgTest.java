package org.example.arrays.countingMinMaxAvg;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CountingMinMaxAvgTest {

    final CountingMinMaxAvg countingMinMaxAvg = new CountingMinMaxAvg();

    @ParameterizedTest
    @MethodSource("prepareArraysMax")
    void countingMax(int[] array, int expected) {
        int result = countingMinMaxAvg.countingMax(array);
        assertEquals(result, expected);
    }

    @ParameterizedTest
    @MethodSource("prepareArraysMin")
    void countingMin(int[] array, int expected) {
        int result = countingMinMaxAvg.countingMin(array);
        assertEquals(result, expected);
    }

    @ParameterizedTest
    @MethodSource("prepareArraysAvg")
    void countingAvg(int[] array, double expected) {
        double result = countingMinMaxAvg.countingAvg(array);
        assertEquals(result, expected);
    }

    public static Stream<Arguments> prepareArraysMax() {
        return Stream.of(
                Arguments.of(new int[]{1, 5, 2, 90, 1}, 90),
                Arguments.of(new int[]{4, 21, 1, 2, 98, 4}, 98),
                Arguments.of(new int[]{55, 33, 11, 2, 1}, 55)
        );
    }

    public static Stream<Arguments> prepareArraysMin() {
        return Stream.of(
                Arguments.of(new int[]{1, 5, 2, 90, 1}, 1),
                Arguments.of(new int[]{4, 21, 1, 2, 98, 4}, 1),
                Arguments.of(new int[]{55, 33, 11, 2, 1}, 1)
        );
    }

    public static Stream<Arguments> prepareArraysAvg() {
        return Stream.of(
                Arguments.of(new int[]{1, 5, 2, 90}, 24.5),
                Arguments.of(new int[]{4, 21, 98, 4}, 31.75)
        );
    }
}
