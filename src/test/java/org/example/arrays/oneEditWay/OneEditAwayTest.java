package org.example.arrays.oneEditWay;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class OneEditAwayTest {

    final OneEditAway oneEditAway = new OneEditAway();

    @ParameterizedTest
    @MethodSource("prepareSource")
    void oneEditWay(String one, String two, boolean expectedResult) {
        //given, when
        boolean isPossible = oneEditAway.oneEditWay(one, two);

        //then
        assertEquals(isPossible, expectedResult);
    }

    public static Stream<Arguments> prepareSource() {
        return Stream.of(
                Arguments.of("tinc", "tanc", true),
                Arguments.of("tnk", "tank", true),
                Arguments.of("tank", "tinck", false),
                Arguments.of("tank", "tankist", false)

                );
    }
}
