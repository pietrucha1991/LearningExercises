package org.example.arrays.shrinkingString;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ShrinkingStringTest {

    final ShrinkingString shrinkingString = new ShrinkingString();

    @ParameterizedTest
    @MethodSource("prepareSource")
    void shrinkingString(String input, String expectedResult) {
        shrinkingString.shrinkingString(input);
    }

    public static Stream<Arguments> prepareSource(){
        return Stream.of(
                Arguments.of("abbb vvvv s rttt rr eeee f", "a1b3 v4 s1 r1t3 r2 e4 f1"),
                Arguments.of("aaabb ccc dddd f", "a3b2 c3 d4 f1")
        );
    }
}
