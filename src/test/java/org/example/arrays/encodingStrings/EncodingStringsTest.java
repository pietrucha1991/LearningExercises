package org.example.arrays.encodingStrings;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class EncodingStringsTest {

    final EncodingStrings encodingStrings = new EncodingStrings();

    @ParameterizedTest
    @MethodSource("prepareCharArray")
    void encodeStrings(char[] word, char[] resultArray) {
        //given, when
        char[] resultToTest = encodingStrings.encodeStrings(word);

        //then
        assertTrue(Arrays.equals(resultToTest, resultArray));
    }

    public static Stream<Arguments> prepareCharArray() {
        char[] str = "  String   with spaces  ".toCharArray();
        char[] result = "%20%20String%20%20%20with%20spaces%20%20".toCharArray();

        return Stream.of(
                Arguments.of(new char[]{'s', ' ', 'd', ' ', 'c'}, new char[]{'s', '%', '2', '0', 'd', '%', '2', '0', 'c'}),
                Arguments.of(new char[]{' ', 's', 'd', 'c'}, new char[]{'%', '2', '0', 's', 'd', 'c'}),
                Arguments.of(str, result)
        );
    }
}
