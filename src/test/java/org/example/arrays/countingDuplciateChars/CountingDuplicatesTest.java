package org.example.arrays.countingDuplciateChars;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class CountingDuplicatesTest {


    final CountingDuplicates classUnderTest = new CountingDuplicates();

    @ParameterizedTest
    @MethodSource("prepareStrings")
    void countDuplicateCharacters(String word, boolean expectedResult) {
        //given, when
        boolean hasDuplicates = classUnderTest.countDuplicateCharacters(word);
        //then
        assertEquals(hasDuplicates, expectedResult);
    }

    public static Stream<Arguments> prepareStrings() {
        return Stream.of(
                Arguments.of("abcd", true),
                Arguments.of("mama", false),
                Arguments.of("123cmd4", true),
                Arguments.of("124casx 312", false),
                Arguments.of("dota", true)
        );
    }


}
