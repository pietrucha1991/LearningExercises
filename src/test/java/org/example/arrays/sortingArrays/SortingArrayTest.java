package org.example.arrays.sortingArrays;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SortingArrayTest {
    final SortingArray sortingArray = new SortingArray();

    @ParameterizedTest
    @MethodSource("prepareSources")
    void sortingArray(int[] ints, int[] expectedResult) {
        //given, when
        int[] sorted = sortingArray.sortingArray(ints);

        //then
        assertTrue(Arrays.equals(sorted, expectedResult));
    }

    public static Stream<Arguments> prepareSources() {
        return Stream.of(
                Arguments.of(new int[]{1, 3, 2, 5, 7, 4, 10}, new int[]{1, 2, 3, 4, 5, 7, 10}),
                Arguments.of(new int[]{1, 3, 2, 3, 7, 1, 20}, new int[]{1, 1, 2, 3, 3, 7, 20}));
    }
}
