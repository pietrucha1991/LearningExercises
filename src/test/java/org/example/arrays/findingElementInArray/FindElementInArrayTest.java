package org.example.arrays.findingElementInArray;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FindElementInArrayTest {

    final FindElementInArray findElementInArray = new FindElementInArray();

    @ParameterizedTest
    @MethodSource("prepareInts")
    void findElementAndReturnIndex(int[] arrayToCheck, int numberToFind, int expectedIndex) {
        //given, when
        int expected = findElementInArray.findElementAndReturnIndex(arrayToCheck, numberToFind);

        //then
        assertEquals(expectedIndex, expected);
    }

    public static Stream<Arguments> prepareInts() {
        return Stream.of(
                Arguments.of(new int[]{1, 33, 2, 3, 1, 6}, 33, 1),
                Arguments.of(new int[]{1, 33, 2, 3, 1, 6}, 22, -1),
                Arguments.of(new int[]{1, 33, 2, 3, 1, 6}, 6, 5)
        );
    }

    @ParameterizedTest
    @MethodSource("prepareStrings")
    void findStringInArrayAndReturnIndex(String[] array, String wordToFind, int expectedIndex) {
        //given, when
        int expected = findElementInArray.findStringInArrayAndReturnIndex(array, wordToFind);

        //then
        assertEquals(expected, expectedIndex);

    }

    public static Stream<Arguments> prepareStrings() {
        return Stream.of(
                Arguments.of(new String[]{"mom", "dad", "dog", "cat"}, "cat", 3),
                Arguments.of(new String[]{"mom", "dad", "dog", "cat"}, "dass", -1),
                Arguments.of(new String[]{"mom", "dad", "dog", "cat"}, "mom", 0));
    }


}
