package org.example.arrays.reversingArray;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ReversingArrayTest {

    final ReversingArray reversingArray = new ReversingArray();

    @Test
    void reverseArray() {
        //given
        int[] array = {3,5,1,2,8,9,6};
        //when
        int[] arraySorted = reversingArray.reverseArray(array);
        //then
        assertTrue(Arrays.equals(new int[]{6,9,8,2,1,5,3}, arraySorted));
    }
}
