package org.example.arrays.uniqueCharacters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UniqueCharactersTest {

    @Test
    void doesContainUniqueCharacters() {
        //given
        String abcd = "abcda";
        //when, then
        assertFalse(UniqueCharacters.doesContainUniqueCharacters(abcd));
    }
    @Test
    void shouldTestAToZChars() {
        //given
        String abcd = "afghnqrsuz";
        //when, then
        assertTrue(UniqueCharacters.doesContainUniqueCharacters(abcd));
    }
}
