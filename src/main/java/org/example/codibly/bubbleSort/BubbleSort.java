package org.example.codibly.bubbleSort;

import java.util.ArrayList;
import java.util.List;

/*
*
Create a “BubbleSort” class with one public method called “sort” (You can create other private methods)
which has to sort numbers in accordance with the “Bubble sort” algorithm.
Method should receive List of Comparable (parameter name = “input”) method should return List of Comparable,
but the numbers should be sorted ascending if the input is null then method should throw an exception
if the input contains null value in list then method should omit this element
Create a “BubbleSortTest” class and create test methods for below cases:
input = [1,4,5,6,8,3,8] result = [1,3,4,5,6,8,8]
input = [-9.3,0.2,4,0.1,5,9] result = [-9.3,0.1,0.2,4,5,9]
input = [] result = []
input = [null,5.0001] result = [5.0001]
input = null result = RuntimeException        */
public class BubbleSort {
    private BubbleSort() {
    }

    public static <T extends Comparable<T>> List<T> sort(List<T> input) {
        if (input.isEmpty()) {
            return input;
        }
        if (input.equals(null)) {
            throw new RuntimeException();
        }
        T temp;

        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < input.size() - 1; i++) {
                if (input.get(i) == null) {
                    input.remove(i);
                } else if (input.get(i).compareTo(input.get(i + 1)) > 0) {
                    temp = input.get(i);
                    input.set(i, input.get(i + 1));
                    input.set(i + 1, temp);
                    sorted = false;
                }
            }
        }
        return input;
    }

}
