package org.example.reccurency.conversions;

public class ToBinary {

    public static void main(String[] args) {
        System.out.println(toBinary(22));
    }

    public static String toBinary(int k) {
        if (k <= 1) {
            return String.valueOf(k);
        }

        int lastDigit = k % 2;
        int remainder = k / 2;
        return toBinary(remainder) + lastDigit;

    }
}
