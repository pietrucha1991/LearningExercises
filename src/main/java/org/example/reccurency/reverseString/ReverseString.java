package org.example.reccurency.reverseString;

public class ReverseString {
    public static void main(String[] args) {
        System.out.println(reverseString("abc"));
    }

    public static String reverseString(String string) {
        if (string.length() <= 1){
            return string;
        }
        final char firstCHar = string.charAt(0);
        String remaining = string.substring(1);

        return reverseString(remaining) + firstCHar;
    }
}
