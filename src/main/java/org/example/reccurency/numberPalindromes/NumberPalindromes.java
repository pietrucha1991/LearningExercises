package org.example.reccurency.numberPalindromes;

public class NumberPalindromes {
    public static void main(String[] args) {
        System.out.println(isPalindrome(171));
        System.out.println(isPalindrome(13));
    }

    public static boolean isPalindrome(int number) {
        boolean result = false;
        if (number < 10) {
            return true;
        }

        int factor = MathUtils.calcPowerOfTen(number);
        int divisor = (int) Math.pow(10, factor);

        if (number < divisor * 10) {
            int leftNumber = number / divisor;
            int rightNumber = number % 10;

            int remainingNumber = (number / 10) % (divisor / 10);
            result =  leftNumber==rightNumber && isPalindrome(remainingNumber);
        }
        return result;
    }
}
