package org.example.reccurency.numberPalindromes;

public class MathUtils {

    static int calcPowerOfTen(int number) {
        return countDigits(number) - 1;
    }

    static int countDigits(int number) {
        int count = 0;
        while (number > 0) {
            number = number / 10;
            count++;
        }
        return count;
    }
}
