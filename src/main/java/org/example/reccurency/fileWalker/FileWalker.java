package org.example.reccurency.fileWalker;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

public class FileWalker {
    public static void main(String[] args) {
        fileWalker("C:\\Users\\lukas\\IdeaProjects\\LearningExercises\\src\\main\\java\\org\\example");
    }
    public static void fileWalker(String path){
        File startFile = new File(path);
        File[] files = startFile.listFiles();

        if(files == null) {
            return;
        }

        for(File f : files){
            if(f.isDirectory()){
                fileWalker(f.getAbsolutePath());
                System.out.println("Dir: " + f.getAbsolutePath());
            }else {
                System.out.println("Files " + f.getAbsoluteFile());
            }
        }

    }
}
