package org.example.reccurency.roboGrid;


// We have an m x n grid. A robot is placed at the top-lef corner of this grid.
//         Te robot can only move either right or down at any point in time, but it is not allowed
//         to move in certain cells. Te robot's goal is to fnd a path from the top-lef corner to the
//         bottom-right corner of the grid.

public class RoboGrid {
    public static void main(String[] args) {
        System.out.println(countPaths(5, 5));
        System.out.println(countPathsMemoization(5, 5));
    }

    //Plain recursion
    public static int countPaths(int m, int n) {
        if (m <= 0 || n <= 0) {
            return -1;
        }
        if (m == 1 || n == 1) {
            return 1;
        }

        return countPaths(m - 1, n) + countPaths(m, n - 1);
    }

    public static int countPathsMemoization(int m, int n) {
        if (m <= 1 || n <= 1) {
            return -1;
        }

        int[][] count = new int[m][n];

        for (int j = 0; j < n; j++) {
            count[0][j] = 1;
        }
        for (int i = 0; i < m; i++) {
            count[i][0] = 1;
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                count[i][j] = count[i-1][j] + count[i][j-1];
            }
        }
        return count[m-1][n-1];

    }


}
