package org.example.reccurency.countDigits;

import java.lang.reflect.Array;

public class CountDigits {
    public static void main(String[] args) {
        System.out.println(countDigits(1234));
        System.out.println(calcSumOfDigits(1234));
    }

    public static int countDigits(int number) {
        if (number < 10) {
            return 1;
        }
        final int remainder = number / 10;
        return countDigits(remainder) + 1;
    }

    public static int calcSumOfDigits(int number) {

        if (number < 10) {
            return number;
        }
        final int remainder = number / 10;
        final int lastDigit = number % 10;
        return calcSumOfDigits(remainder) + lastDigit;
    }

    public static int countSum(int n) {
        if (n == 0 || n == 1) {
            return 1;
        }
        return n + countSum(n-1);
    }
    /*
    *countSum(5) ->
    * 5 + countSum(4) -> 5 + 10 = 15;
    * 4 + countSum(3) -> 4 + 6 = 10
    * 3 + countSum(2) -> 3 + 3 = 6
    * 2 + countSum(1) -> 2 + 1 = 3
    * **/
}
