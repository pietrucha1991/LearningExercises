package org.example.reccurency.arraySum;

public class ArraySum {
    public static void main(String[] args) {
        System.out.println(sumArray(new int[]{1, 2, 3, 4}));
    }

    public static int sumArray(int[] array) {
        return sum(array, 0);
    }

    public static int sum(int[] values, int position) {
        if (position >= values.length) {
            return 0;
        }
        int value = values[position];
        return value + sum(values, position + 1);
    }
}
