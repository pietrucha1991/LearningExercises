package org.example.reccurency.GCD;

public class GCDRecursive {
    public static void main(String[] args) {
        System.out.println(gcd(42, 28));
    }

    public static int gcd(int a, int b) {
        if(b == 0){
            return a;
        }
        return gcd(b, a%b);
    }
}
