package org.example.reccurency.palindrome;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Palindrome {
    public static boolean isPalindrome(String input) {
        StringBuilder sb = new StringBuilder(input);

        for (int i = input.length() - 1; i >= 0; i--) {
            sb.append(input.charAt(i));
        }

        return input.equals(sb.reverse().toString());
    }

}
