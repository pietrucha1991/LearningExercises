package org.example.reccurency.isPower;

public class isPowerOfThree {
    public static void main(String[] args) {
        System.out.println(isPowerOfThree(3));
        System.out.println(isPowerOfThree(15));
        System.out.println(isPowerOfThree(24));
        System.out.println(isPowerOfThree(27));
    }

    public static boolean isPowerOfThree(int n) {
        boolean result = false;
        if (n < 3) {
            return n == 1 || n == 2;
        }
        if(n % 3 == 0){
            result =  isPowerOfThree(n / 3);
        }
        return result;
    }
}
