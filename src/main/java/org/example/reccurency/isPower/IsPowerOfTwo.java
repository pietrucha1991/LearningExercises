package org.example.reccurency.isPower;

public class IsPowerOfTwo {
    public static void main(String[] args) {
        System.out.println(isPowerOf2(10));
        System.out.println(isPowerOf2(1));
        System.out.println(isPowerOf2(2));

    }

    public static boolean isPowerOf2(int n) {

        boolean result = false;
        if (n < 2) {
            return n == 1;
        }
        if (n % 2 == 0) {
            result = isPowerOf2(n / 2);
        }
        return result;
    }
}
