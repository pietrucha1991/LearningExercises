package org.example.reccurency.isPower;

public class PowerOf {
    public static void main(String[] args) {
        System.out.println(powerOf(4, 2));
        System.out.println(powerOf(2, 8));
    }

    public static long powerOf(int value, int exponent) {
        if (exponent < 0) {
            throw new RuntimeException();
        }
        //recursive termination
        if (exponent == 0) {
            return 1;
        }
        if (exponent == 1) {
            return value;
        }
        //recursive descent
        return value * powerOf(value, exponent - 1);

    }
}
