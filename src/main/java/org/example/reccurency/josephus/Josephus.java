package org.example.reccurency.josephus;

public class Josephus {
    public static void main(String[] args) {

    }

    public static int findPosition(int n, int k) {
        if (n == 1) {
            return 1;
        } else {
            return (findPosition(n - 1, k) + k - 1) %n - 1;
        }

    }
}
