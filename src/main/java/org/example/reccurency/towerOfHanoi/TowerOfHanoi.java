package org.example.reccurency.towerOfHanoi;

public class TowerOfHanoi {
    public static void main(String[] args) {
       moveDisks(4, 'A', 'B', 'C');
    }

    public static void moveDisks(int n, char origin, char target, char intermediate) {
        if (n <= 0) {
            return;
        }
        if (n == 1){
            System.out.println("Move disk 1 from " + origin + " to target " + target);
            return;
        }

        moveDisks(n -1, origin, intermediate, target);
        System.out.println("Moving disk " + n + " from rod " + origin + " to rod " + target);
        moveDisks(n -1, intermediate, target, origin);

    }
}
