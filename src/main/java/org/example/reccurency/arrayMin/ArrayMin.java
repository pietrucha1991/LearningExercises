package org.example.reccurency.arrayMin;

public class ArrayMin {
    public static void main(String[] args) {
        System.out.println(arrayMin(new int[]{1,3,5,3,2,1,0,9}));
    }

    public static int arrayMin(int[] array){
        return min(array, 0, Integer.MAX_VALUE);
    }

    static int min(int[] values, int position, int curentMin){
        if(position >= values.length){
            return curentMin;
        }
        int current = values[position];
        if(current < curentMin){
            curentMin = current;
        }
        return min(values, position + 1, curentMin);
    }
}
