package org.example.reccurency.fibonacci;

public class Fibonacci {
    public static void main(String[] args) {
        System.out.println(fibonacciPlainRecursion(4));
        System.out.println(fibonacciMemoization(4));
        System.out.println(fibonacciTabulation(4));
    }

    public static int fibonacciPlainRecursion(int k) {
        if (k <= 1) {
            return k;
        }

        return fibonacciPlainRecursion(k - 2) + fibonacciPlainRecursion(k - 1);
    }
    /*
    *                                   fibonacci(5)
    *             fibonacci(3)                +                 fibonacci(4)
    * fibonacci(1)    +       fibonacci(2)       || fibonacci(2)     +        fibonacci(3)
    *     1          fibonacci(0) + fibonacci(1) ||      1                         2
    *     1               0             1        ||      1                         2        ->  5
    * */


    static int fibonacciMemoization(int k) {
        return fibonacci(k, new int[k + 1]);
    }

    static int fibonacci(int k, int[] cache) {
        if (k <= 1) {
            return k;
        } else if (cache[k] > 0) {
            return cache[k];
        }
        cache[k] = fibonacci(k - 2, cache) + fibonacci(k - 1, cache);
        return cache[k];
    }

    static int fibonacciTabulation(int k) {
        if (k <= 1) {
            return k;
        }
        int first = 1;
        int second = 0;
        int result = 0;

        for (int i = 1; i < k; i++) {
            result = first + second;
            second = first;
            first = result;
        }
        return result;
    }

}
