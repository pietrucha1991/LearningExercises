package org.example.arrays.oneEditWay;

public class OneEditAway {


    public boolean oneEditWay(String one, String two) {
        if (Math.abs(one.length() - two.length()) > 1 || one.equals(two)) {
            return false;
        }

        String shorterString = one.length() < two.length() ? one : two;
        String longerString = one.length() < two.length() ? two : one;

        int is = 0;
        int il = 0;
        boolean marker = false;

        while (is < shorterString.length() && il < longerString.length()) {

            if (shorterString.charAt(is) != longerString.charAt(il)) {
                if (marker) {
                    return false;
                }
                marker = true;
                if (shorterString.length() == longerString.length()) {
                    is++;
                }
            } else {
                is++;
            }
            il++;
        }
        return true;
    }
}
