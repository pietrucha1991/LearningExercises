package org.example.arrays.shrinkingString;

public class ShrinkingString {

    public String shrinkingString(String input) {
        int count = 0;

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            count++;

            if (!Character.isWhitespace(input.charAt(i))) {
                if ((i + 1) >= input.length() || input.charAt(i) != input.charAt(i + 1)) {
                    sb.append(input.charAt(i)).append(count);
                    count = 0;
                }
            } else {
                sb.append(input.charAt(i));
                count = 0;
            }
        }

        return sb.length() > input.length() ? input : sb.toString();
    }
}
