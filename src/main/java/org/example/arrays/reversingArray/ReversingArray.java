package org.example.arrays.reversingArray;

import java.util.Arrays;
import java.util.stream.IntStream;

public class ReversingArray {

    public int[] reverseArray(int[] array){
        return IntStream.rangeClosed(1, array.length).map(i -> array[array.length - i]).toArray();
    }
}
