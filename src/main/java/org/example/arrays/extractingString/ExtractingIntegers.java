package org.example.arrays.extractingString;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

public class ExtractingIntegers {

    public List<Integer> extractingIntegers(String input) {
        List<Integer> result = new ArrayList<>();
        StringBuilder sb = new StringBuilder(String.valueOf(Integer.MAX_VALUE).length());

        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i))) {
                sb.append(input.charAt(i));
            } else {
                if (sb.length() > 0) {
                    result.add(Integer.parseInt(sb.toString()));
                    sb.delete(0, sb.length());
                }
            }
        }

        return result;
    }
}
