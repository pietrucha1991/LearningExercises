package org.example.arrays.encodingStrings;

public class EncodingStrings {

    public char[] encodeStrings(char[] charToEncode) {
        int count = 0;
        for (int i = 0; i < charToEncode.length; i++) {
            if (charToEncode[i] == ' ') {
                count++;
            }
        }
        char[] newArray = new char[charToEncode.length + count * 2];

        if (count > 0) {
            int index = 0;
            for (int i = 0; i < charToEncode.length; i++) {
                if (Character.isWhitespace(charToEncode[i])) {
                    newArray[index] = '%';
                    newArray[index + 1] = '2';
                    newArray[index + 2] = '0';
                    index += 3;
                } else {
                    newArray[index] = charToEncode[i];
                    index++;
                }
            }
        }
        return newArray;
    }
}
