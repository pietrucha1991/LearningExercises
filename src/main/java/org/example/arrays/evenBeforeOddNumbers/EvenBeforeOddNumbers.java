package org.example.arrays.evenBeforeOddNumbers;

public class EvenBeforeOddNumbers {

    /*
     * To do this task we have to
     * 1. Check if they are even or odd
     * 2. swap numbers
     * 3. return the array

     * */

    public int[] evenBeforeOddNumbers(int[] array) {

        int count = 0;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (!isEven(array[j])) {
                    final int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
                count++;
            }
        }

        return array;
    }

    //Helper method for checking if its odd or even
    private boolean isEven(int number) {
        return number % 2 == 0;
    }
}
