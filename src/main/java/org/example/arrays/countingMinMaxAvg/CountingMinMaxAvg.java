package org.example.arrays.countingMinMaxAvg;

import java.util.Arrays;

public class CountingMinMaxAvg {

    public int countingMax(int[] array) {
        return Arrays.stream(array).max().getAsInt();
    }

    public int countingMin(int[] array) {
        return Arrays.stream(array).min().getAsInt();
    }

    public double countingAvg(int[] array) {
        return Arrays.stream(array).average().getAsDouble();
    }
}
