package org.example.arrays.uniqueCharacters;

import java.util.HashMap;
import java.util.Map;

public class UniqueCharacters {

    static boolean doesContainUniqueCharacters(String input) {
        Map<Character, Boolean> map = new HashMap<>();

        for (int i = 0; i < input.length(); i++) {
            if (input.codePointAt(i) >= 97 && input.codePointAt(i) <= 122) {
                char ch = input.charAt(i);
                if (!Character.isWhitespace(ch)) {
                    if (map.put(ch, true) != null) {
                        return false;
                    }
                }
            } else {
                System.out.println("Given string conaints unallowed chars");
                return false;
            }

        }
        return true;
    }
}
