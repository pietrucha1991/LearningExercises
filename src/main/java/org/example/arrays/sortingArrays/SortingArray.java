package org.example.arrays.sortingArrays;

public class SortingArray {

    public int[] sortingArray(int[] arrayToSort) {
        int temp = 0;

        for (int i = 0; i < arrayToSort.length - 1; i++) {
            for (int j = 0; j < arrayToSort.length - 1; j++) {
                if (arrayToSort[j] > arrayToSort[j + 1]) {
                    temp = arrayToSort[j + 1];
                    arrayToSort[j + 1] = arrayToSort[j];
                    arrayToSort[j] = temp;
                }
            }

        }
        return arrayToSort;
    }

}
