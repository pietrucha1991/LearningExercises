package org.example.arrays.flip.flipHorizontally;

public class FlipHorizontally {


    static void flipHorizontally(Integer[][] array) {
        for (int i = 0; i < array.length; i++) {
            final int endPos = array[i].length;

            int leftIndx = 0;
            int rightIndx = endPos - 1;

            while(leftIndx <  rightIndx){
                int leftValue = array[i][leftIndx];
                int rightValue = array[i][rightIndx];

                array[i][leftIndx] = rightValue;
                array[i][rightIndx] = leftValue;

                leftIndx++;
                rightIndx--;
            }

        }
    }
}
