package org.example.arrays.findingElementInArray;

//find element and return its index.
public class FindElementInArray {

    public int findElementAndReturnIndex(int[] array, int number) {

        for (int i = 0; i < array.length; i++) {
            if (number == array[i]) {
                return i;
            }
        }

        return -1;
    }

    public int findStringInArrayAndReturnIndex(String[] array, String toFind) {
        for (int i = 0; i < array.length; i++) {
            if (toFind.equals(array[i])) {
                return i;
            }
        }
        return -1;
    }
}
