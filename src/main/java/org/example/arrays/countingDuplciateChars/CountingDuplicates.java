package org.example.arrays.countingDuplciateChars;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CountingDuplicates {

    public boolean countDuplicateCharacters(String word) {
        Set<Character> set = new HashSet<>();

        for (int i = 0; i < word.length(); i++) {
            char character = word.charAt(i);
            set.add(character);
        }

        if(set.size() == word.length()){
            return true;
        }else {
            return false;
        }
    }
}
