package org.example.tasks.TenBeginnerCodeChallenges.wordReversal;


/*
 *
 * For this challenge, the input is a string of words, and the output should be the words in reverse but with the letters in the original order.
 * For example, the string “Dog bites man” should output as “man bites Dog.”
 * After you’ve solved this challenge, try adding sentence capitalization and punctuation to your code.
 * So, the string “Codecademy is the best!” should output as “Best the is Codecademy!”*/

import java.util.Set;

public class WordReversal {

    private static final Set<String> punctuations = Set.of("!", "?", ",", ".");

    public static String reverse(String input) {

        String punctuation = findPunctuation(input);
        String[] inputArray = input.replaceAll("\\p{Punct}", "").toLowerCase().split(" ");

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = inputArray.length - 1; i >= 0; i--) {
            stringBuilder.append(inputArray[i]);
            if (i != 0) {
                stringBuilder.append(" ");
            }
        }

        return stringBuilder.substring(0, 1).toUpperCase() + stringBuilder.substring(1) + punctuation;
    }

    private static String findPunctuation(String input) {
        String lastChar = input.substring(input.length() - 1);
        return punctuations.contains(lastChar) ? lastChar : "";
    }


}
