package org.example.tasks.TenBeginnerCodeChallenges.pangrams;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
 * A pangram is a sentence that contains all 26 letters of the English alphabet. One of the most well-known examples of a pangram is,
 * “The quick brown fox jumps over the lazy dog.”
 * Create a pangram checker that returns a Boolean TRUE if an input string is a pangram and FALSE if it isn’t.
 *
 * For an added pangram challenge, create a perfect pangram checker.
 * A perfect pangram is a sentence that uses each letter of the alphabet only once, such as,
 * “Mr. Jock, TV quiz Ph.D., bags few lynx.”
 * */
public class Pangrams {

    public static boolean isPangram(String input) {
        input = input.toLowerCase().replaceAll(" ", "").replaceAll("\\p{Punct}", "");
        List<Character> alphabet = IntStream.rangeClosed('a', 'z').mapToObj(var -> (char) var).collect(Collectors.toList());

        for (int i = 0; i < input.length(); i++) {
            if (!alphabet.contains(input.charAt(i))) {
                return false;
            }
        }

        return true;
    }


    public static boolean isPerfectPangram(String inputString) {
        //create alphabet
        List<Character> alphabet = IntStream.rangeClosed('a', 'z').mapToObj(var -> (char) var).collect(Collectors.toList());

        //edit string to get onli letters
        inputString = inputString.toLowerCase().replaceAll(" ", "").replaceAll("\\p{Punct}", "");

        //create map to store letters and occurrences
        Map<Character, Integer> map = new HashMap<>();

        //use for loop to check the occurrences- if letter occurs more than once it's not perfect pangram.
        for (int i = 0; i < inputString.length(); i++) {
            if (alphabet.contains(inputString.charAt(i)) && !map.containsKey(inputString.charAt(i))) {
                map.put(inputString.charAt(i), 1);
            } else {
                return false;
            }
        }

        return true;
    }


}
