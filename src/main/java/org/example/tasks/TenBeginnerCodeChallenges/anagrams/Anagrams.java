package org.example.tasks.TenBeginnerCodeChallenges.anagrams;


import java.util.*;

/*
* Two words are anagrams if they contain the same letters but in a different order. Here are a few examples of anagram pairs:
    “listen” and “silent”
    “binary” and “brainy”
    “Paris” and “pairs”

* For a given input of two strings, return a Boolean TRUE if the two strings are anagrams.
* As an added challenge, for `a given array of strings, return separate lists that group anagrams together.
* For example, the input {“tar,” “rat,” “art,” “meats,” “steam”},
* the output should look something like {[“tar,” “rat,” “art”], [“meats,” “steam”]}.
* */
public class Anagrams {

    public static boolean isAnagram(String firstWord, String secondWord) {
        char[] firstWordArr = firstWord.toLowerCase().toCharArray();
        char[] secondWordArr = secondWord.toLowerCase().toCharArray();

        Arrays.sort(firstWordArr);
        Arrays.sort(secondWordArr);

        return Arrays.equals(firstWordArr, secondWordArr);
    }

    public static List<List<String>> groupAnagramsNoSet(List<String> input) {

        List<List<String>> anagramLists = new ArrayList<>();
        int index = 0;
        while (index < input.size()) {
            List<String> group = new ArrayList<>();

            for (int i = 0; i < input.size(); i++) {
                if (isAnagram(input.get(index), input.get(i))) {
                    group.add(input.get(i));
                }

            }
            anagramLists.add(group);
            index+=group.size();
        }

        return anagramLists;
    }


    public static List<List<String>> groupAnagramsUsingSet(List<String> input) {
        Set<List<String>> anagramsLists = new HashSet<>();

        for (String word : input) {
            List<String> group = new ArrayList<>();
            for (int i = 0; i < input.size(); i++) {
                if (isAnagram(word, input.get(i))) {
                    group.add(input.get(i));
                }
            }
            anagramsLists.add(group);

        }

        return new ArrayList<>(anagramsLists);
    }
}
