package org.example.tasks.TenBeginnerCodeChallenges.numberReversal;

/*
 * This one is a technical interview favorite. For a given input number, return the number in reverse.
 * So, an input of 3956 should return 6593.
 * If you’re ready for a bigger challenge, reverse a decimal number.
 * The decimal point should stay in the same place. So, the number 193.56 should output 653.91.
 * */
public class NumberReversal {

    static int numberReversal(int number) {
        StringBuilder sb = new StringBuilder(String.valueOf(number));

        String result = sb.reverse().toString();
        return Integer.parseInt(result);
    }

    static double decimalNumberReversal(double number) {
        String numberString = String.valueOf(number);
        int decimalMarkPosition = numberString.indexOf(".");
        char decimalMark = numberString.charAt(decimalMarkPosition);

        numberString = numberString.replaceAll("[,-.]", "");
        char[] inputArray = numberString.toCharArray();
        StringBuilder sb = new StringBuilder();

        for (int i = inputArray.length - 1; i >= 0; i--) {
            sb.append(inputArray[i]);
        }

        return Double.parseDouble(sb.substring(0, decimalMarkPosition) + decimalMark + sb.substring(decimalMarkPosition));
    }
}
