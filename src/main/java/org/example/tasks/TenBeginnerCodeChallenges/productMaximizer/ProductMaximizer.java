package org.example.tasks.TenBeginnerCodeChallenges.productMaximizer;


import java.util.Arrays;

/*
 * For a given input array of numbers, find the two that result in the largest product.
 * The output should include the two numbers in the array along with their product.
 * As an extra challenge, use an input of two arrays of numbers and find two numbers
 * — one from each input array — that results in the largest product.*/
public class ProductMaximizer {

    static int[] ProductMaximizer(int[] input) {
        int[] result = new int[3];

        Arrays.sort(input);

        result[0] = input[input.length - 1];
        result[1] = input[input.length - 2];
        result[2] = result[0] * result[1];

        return result;
    }

    static int[] ProductMaximizer(int[] inputOne, int[] inputTwo) {
        int[] result = new int[3];

        Arrays.sort(inputOne);
        Arrays.sort(inputTwo);

        result[0] = inputOne[inputOne.length - 1];
        result[1] = inputTwo[inputTwo.length - 1];
        result[2] = result[0] * result[1];

        return result;
    }
}
