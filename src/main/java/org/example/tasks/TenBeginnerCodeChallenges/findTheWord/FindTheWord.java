package org.example.tasks.TenBeginnerCodeChallenges.findTheWord;


/*
 * Starting with an input string of words, find the second-to-last word of the string.
 * For example, an input of “I love Codecademy” should return “love.”
 * To make your program more challenging, allow for a second numerical input, n, that results in returning the nth word of a string.
 * So, for the string “I can program in Java” and n = 3, the output should be the third word, “program.”
 * */
public class FindTheWord {

    public static String findSecondToLastWord(String input) {
        //create array of words
        String[] inputArray = input.split(" ");

        //index of second to last element
        int secondToLastIndex = inputArray.length - 2;
        if (secondToLastIndex < 0) {
            return "";
        }

        //return the word from array
        return inputArray[secondToLastIndex];
    }

    public static String findWordAtPosition(String input, int wordIndexToFind) {
        String[] inputArray = input.split(" ");

        return inputArray[wordIndexToFind - 1];
    }

}
