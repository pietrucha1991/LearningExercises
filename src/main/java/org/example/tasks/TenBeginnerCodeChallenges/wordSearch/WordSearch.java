package org.example.tasks.TenBeginnerCodeChallenges.wordSearch;


/*
 * For a given input string, return a Boolean TRUE if the string starts with a given input word.
 * So, for an input string of “hello world” and input word “hello,” the program should return TRUE.
 * For a more advanced word searcher, create a program that returns the number of times a word appears in an input string.
 * For example, given an input word “new” and an input string “I’m the new newt,” the program should return a value of 2.*/

public class WordSearch {

    public static boolean startsWith(String input, String toFind) {
        String firstWord = input.substring(0, input.indexOf(" "));

        if (firstWord.equals(toFind)) {
            return true;
        }

        return false;
    }

    public static int countOccurrences(String input, String toFind) {
        int count = 0;
        String[] inputArray = input.split(" ");

        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i].contains(toFind)) {
                count++;
            }
        }
        return count;
    }

}
