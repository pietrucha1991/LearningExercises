package org.example.tasks;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TextTasks {


    //as name suggests.. ignore lower/upper case. We only care about words so ignore ! , . - etc`
    public Map<String, Integer> countWords(final String input) {

        String[] inputStringArray = input
                .toLowerCase()
                .replaceAll("[,-.]", "")
                .split(" ");

        Map<String, Integer> counter = new HashMap<>();
        for(String word : inputStringArray){
            if(counter.containsKey(word)){
                counter.put(word, counter.get(word) + 1);
            }else {
                counter.put(word, 1);
            }
        }
        return counter;
    }

    public Collection<String> lookupText(final String input, final String searchedText) {
        throw new RuntimeException("not implemented");
    }
}
