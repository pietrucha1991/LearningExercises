package org.example.tasks;


public class MySimpleList<T> {

    private int lastIndex = 0;
    private Object[] inner; // use this and simply use casting of objects. IMO it will be the simpliest option for you

    public MySimpleList() {
        inner = new Object[10];
    }

    public MySimpleList(int capacity) {
        inner = new Object[capacity];
    }

    public void add(T element) {
        if (lastIndex >= inner.length) {
            enlargeArray();
        }
        addElementToArray(element);
    }

    private void enlargeArray() {
        Object[] tempArray = new Object[inner.length * 2];
        for (int i = 0; i < inner.length; i++) {
            tempArray[i] = inner[i];
        }
        inner = tempArray;
    }

    private void addElementToArray(T element) {
        inner[lastIndex] = element;
        lastIndex++;
    }

    public void remove(final int indexToRemove) {
        lastIndex--;
        if (indexToRemove == lastIndex) {
            inner[indexToRemove] = null;
        } else {
            for (int i = indexToRemove; i < inner.length - 1; i++) {
                if (inner[i] == null) {
                    break;
                }
                inner[i] = inner[i + 1];
            }
        }
    }

    public T get(final int indexToGet) {
        if (indexToGet >= lastIndex) {
            throw new IndexOutOfBoundsException();
        }
        return (T) inner[indexToGet];
    }

    public int size() {
        return lastIndex;
    }
}
