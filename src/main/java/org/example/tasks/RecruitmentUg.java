package org.example.tasks;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RecruitmentUg {

    public int findSubArray(final int[][] array, final int[] subArray) {
        for (int i = 0; i < array.length; i++) {
            if(loopyDeepEquals(array[i], subArray)){
                return i;
            }
        }

        return -1;
    }

    public int findSubArray(final int[] array, final int[] subArray) {
        if (array.length == 0 || subArray.length == 0) {
            return -1;
        }

        int count = 0;
        for (int i = 0; i < array.length; i++) {

            if (array[i] == subArray[count]) {
                count++;
            } else {
                count = 0;
            }
            if (count == subArray.length) {
                return i - count + 1;
            }
        }

        return -1;
    }


    public Map<String, String> getUrlParameters(final String url) {
        Map<String, String> result = new HashMap<>();
        String[] urlAndParams = url.split("\\?");

        if (urlAndParams.length == 1 || url.equals("") || urlAndParams.length > 2) {
            return result;
        }


        Arrays.stream(urlAndParams[1].split("&")).forEach((pair) -> {
            String[] array = pair.split("=");
            result.put(array[0], array[1]);
        });

        return result;
    }

    //using only loops check if two arrays have the same content
    public boolean loopyDeepEquals(final int[] array1, final int[] array2) {
        if (array1.length != array2.length) {
            return false;
        }

        for (int i = 0; i < array1.length; i++) {
            if (array1[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }
}


