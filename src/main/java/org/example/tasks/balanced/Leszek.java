package org.example.tasks.balanced;

public class Leszek implements BalancedWords {

    public static final int A_VALUE = Character.getNumericValue('a');

    @Override
    public int countBalancedWords(String inputWord) {

        //EXCEPTION CHECK
        if (inputWord == null) throw new RuntimeException("text is null");
        if (inputWord.matches(".*[0-9]+.*")) throw new RuntimeException("text contains digits");

        //Dim inputWord As String:inputWord = CStr(inputWordVar)


        // No exception - count it
        int n = inputWord.length();

        // WORD IS EMPTY STRING
        if (n == 0) return 0;


        // WORD IS SHORTER THAN 3 CHARS
        if (n < 3) return 2 * n - 1;

 /*       ' CALCLUATING THREE AND MORE character WORDS
        ' ________________________________________________
        ' Parameter: total number of characters possible in the word
        ' (would be bigger if we allowed big letters, other chars, etc)*/
        int result = 0;
        int nPossibleChars = 26;

        int arraySize;
        int[] calculationArray;
        char characterAtPosition;
        int i;
        int startPosition, endPosition;

/*        ' Array for counting characters and calculating totals by index.
        ' (We make it single-dimensional for fastest possible memory allocation.
        ' We could make it 2-dimensional for better code readability, but then in Java
        ' we would have to call n allocations of sub-arrays. The second dimension
        ' would be characters - we mark it here by index name: charDimensionIndex.)*/
        arraySize = n * nPossibleChars;
        calculationArray = new int[arraySize];

        //' this replaces array' s second dimension
        int charDimensionIndex, charOffset;

        // ' fill the array (computational complexity = n)
        for (startPosition = 0; startPosition < n; startPosition++) {
            characterAtPosition = inputWord.charAt(startPosition);
            charOffset = (Character.getNumericValue(characterAtPosition) - A_VALUE) * n;
            i = charOffset + startPosition;
            calculationArray[i] = 1;
        }

        //calculate compound totals at indexes (computational complexity = n)
        for (charDimensionIndex = 0; charDimensionIndex < nPossibleChars; charDimensionIndex++) {
            for (startPosition = 1; startPosition < n; startPosition++) { // ' note: we skip index=0 because the total there does not change
                charOffset = charDimensionIndex * n;
                i = charOffset + startPosition;
                calculationArray[i] = calculationArray[i] + calculationArray[i - 1];
            }
        }

        // count balanced words (computational complexity = n2)
        int nTimesNext, nTimesPrev;
        boolean isBalanced;

        int preStartTotal; // last total before current substring' s start index
        int endTotal; // total at the substring' s end index

        // iterate all 3-and-more letters words
        for (startPosition = 0; startPosition < (n - 2); startPosition++) {
            for (endPosition = startPosition + 2; endPosition < n; endPosition++) {

                // check if the word between start position and end position is balanced
                isBalanced = true;
                nTimesPrev = 0;

                // iterate characters
                for (charDimensionIndex = 0; charDimensionIndex < nPossibleChars; charDimensionIndex++) {
                    charOffset = charDimensionIndex * n;

                    if (startPosition == 0) {
                        preStartTotal = 0;
                    } else {
                        preStartTotal = calculationArray[charOffset + startPosition - 1];
                    }

                    endTotal = calculationArray[charOffset + endPosition];

                    nTimesNext = endTotal - preStartTotal;

                    if (nTimesNext != 0) {
                        if (nTimesPrev == 0) {
                            nTimesPrev = nTimesNext;
                        } else if (nTimesNext != nTimesPrev) {
                            isBalanced = false;
                            break;
                        }
                    }

                }

                if (isBalanced) {
                    result = result + 1;
                }
            }

        }

        // add 1-letter and 2-letter words
        result = result + n * 2 - 1;

        return result;
    }

    @Override
    public String name() {
        return "Leszek 1.0";
    }

    /*
    *
    * Public Function Count(Optional inputWordVar As Variant) As Integer

    ' EXCEPTION CHECK
    ' ________________________________________________
    If IsMissing(inputWordVar) Then
        ThrowException "text is null"
        Exit Function
    End If

    Dim inputWord As String: inputWord = CStr(inputWordVar)
    If RegExTest(text:=inputWord, txPattern:=".*[0-9]+.*") Then
        ThrowException "text contains digits"
        Exit Function
    End If


    ' No exception - count it
    ' ________________________________________________
    Dim n As Integer
    n = Len(inputWord)


    ' WORD IS EMPTY STRING
    ' ________________________________________________
    If n = 0 Then
        Count = 0
        Exit Function
    End If


    ' WORD IS SHORTER THAN 3 CHARS
    ' ________________________________________________
    If n < 3 Then
        Count = 2 * n - 1
        Exit Function
    End If


    ' CALCLUATING THREE AND MORE character WORDS
    ' ________________________________________________
    ' Parameter: total number of characters possible in the word
    ' (would be bigger if we allowed big letters, other chars, etc)
    Dim nPossibleChars As Integer
    nPossibleChars = 26

    Dim arraySize As Integer, calculationArray() As Integer
    Dim characterAtPosition As String
    Dim i As Integer
    Dim startPosition As Integer, endPosition As Integer


    ' Array for counting characters and calculating totals by index.
    ' (We make it single-dimensional for fastest possible memory allocation.
    ' We could make it 2-dimensional for better code readability, but then in Java
    ' we would have to call n allocations of sub-arrays. The second dimension
    ' would be characters - we mark it here by index name: charDimensionIndex.)
    arraySize = n * nPossibleChars
    ReDim calculationArray(arraySize)

    ' this replaces array's second dimension
    Dim charDimensionIndex As Integer, charOffset As Integer

    ' fill the array (computational complexity = n)
    For startPosition = 0 To (n - 1)
        characterAtPosition = Mid(inputWord, startPosition + 1, 1)  ' in function Mid fist index = 1
        charOffset = (Asc(characterAtPosition) - Asc("a")) * n
        i = charOffset + startPosition
        calculationArray(i) = 1
    Next

    ' calculate compound totals at indexes (computational complexity = n)
    For charDimensionIndex = 0 To (nPossibleChars - 1)
        For startPosition = 1 To (n - 1)   ' note: we skip index=0 because the total there does not change
            charOffset = charDimensionIndex * n
            i = charOffset + startPosition
            calculationArray(i) = calculationArray(i) + calculationArray(i - 1)
        Next
    Next

    ' count balanced words (computational complexity = n2)
    Dim nTimesNext As Integer, nTimesPrev As Integer, isBalanced As Boolean

    Dim preStartTotal As Integer    ' last total before current substring's start index
    Dim endTotal  As Integer        ' total at the substring's end index

    ' iterate all 3-and-more letters words
    For startPosition = 0 To (n - 3)
        For endPosition = startPosition + 2 To n - 1

            ' check if the word between start position and end position is balanced
            isBalanced = True
            nTimesPrev = 0

            ' iterate characters
            For charDimensionIndex = 0 To (nPossibleChars - 1)

                charOffset = charDimensionIndex * n

                If startPosition = 0 Then
                    preStartTotal = 0
                Else
                    preStartTotal = calculationArray(charOffset + startPosition - 1)
                End If

                endTotal = calculationArray(charOffset + endPosition)

                nTimesNext = endTotal - preStartTotal

                If nTimesNext <> 0 Then
                    If nTimesPrev = 0 Then
                        nTimesPrev = nTimesNext
                    ElseIf nTimesNext <> nTimesPrev Then
                        isBalanced = False
                        Exit For
                    End If
                End If
            Next

            If isBalanced Then Count = Count + 1
        Next
    Next

    ' add 1-letter and 2-letter words
    Count = Count + n * 2 - 1

End Function
    *
    * */
}
