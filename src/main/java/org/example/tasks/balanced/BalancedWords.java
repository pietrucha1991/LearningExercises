package org.example.tasks.balanced;

public interface BalancedWords {


    int countBalancedWords(final String input);

    String name();
}
