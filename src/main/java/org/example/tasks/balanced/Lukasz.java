package org.example.tasks.balanced;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
## Balanced words definition:
The word is balanced if every letter that occurs in it occurs in the same number of time.
E.g. the words: “mama”, “ear”, “aabbcbcccbaa” are balanced but the words “dad”, “elephant”, “abcba” are not.
Additionally, the balanced word is not empty.
Create a “BalancedWordsCounter” class with one public method called “count” (you can create other private methods)
which has to count how many balanced subwords exist in the input word. method should receive String (parameter name = “input”)
method should return Integer the input should contain the letters only, throw an exception if the text contains other characters.
if the input is null then method should throw an exception
Create a “BalancedWordsCounterTest” and create a test methods for below cases:
input = “aabbabcccba” result = 28
input = “” result = 0
input = “abababa1” result = RuntimeException
input = null result = RuntimeException *
*/
public class Lukasz  implements BalancedWords {

    @Override
    public int countBalancedWords(String input) {
        if (input == null || input.matches(".*\\d.*")) {
            throw new RuntimeException();
        } else if (input.equals("")) {
            return 0;
        }

        List<String> subwords = generateSubwords(input);
        int counter = 0;

        for (String word : subwords) {
            if (isBalanced(word)) {
                counter++;
            }
        }
        return counter;
    }

    @Override
    public String name() {
        return "Lukasz 1.0";
    }

    private boolean isBalanced(String word) {
        Set<Integer> allOccurrences = new HashSet<>();

        int count = 0;
        for (int i = 0; i < word.length(); i++) {
            for (int j = 0; j < word.length(); j++) {
                if (word.charAt(i) == word.charAt(j)) {
                    count++;
                }
            }
            allOccurrences.add(count);
            count = 0;
            if (allOccurrences.size() > 1) {
                break;
            }
        }
        return allOccurrences.size() == 1;
    }

    private List<String> generateSubwords(String word) {
        List<String> subWords = new ArrayList<>();

        for (int i = 0; i < word.length(); i++) {
            for (int j = i + 1; j <= word.length(); j++) {
                String subWord = word.substring(i, j);
                subWords.add(subWord);
            }
        }

        return subWords;
    }

}

