package org.example.tasks.balanced;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class Maciek2 implements BalancedWords {

    private static final String ANY_DIGIT = ".*\\d+.*";

    @Override
    public int countBalancedWords(final String input) {
        validateInput(input);
        if (input.length() == 0) {
            return 0;
        }

        final String[] byLetters = input.split("");

        final Map<String, Integer> countByCharacter = Arrays.stream(byLetters)
                .collect(Collectors.toMap(charToCount -> charToCount, charToCount -> 0, (r1, r2) -> r1));

        int result = 0; //countGuaranteedResult(input); restore for optimasition


        for (int startPoint = 0; startPoint < byLetters.length; startPoint++) {
            resetCounter(countByCharacter);

            for (int index = startPoint; index < byLetters.length; index++) {
                countByCharacter.compute(byLetters[index], (key, val) -> ++val);
                if (index == 0 || index == 1) { //1 and 2 letter words will always be balan
                    result++;
                    continue;
                }
                if (isBalanced(countByCharacter, byLetters[index])) {
                    result++;
                }
            }
        }

        return result; //253 1774  154990
    }

    private static void validateInput(final String input) {
        if (input == null) {
            throw new RuntimeException("Input is null");
        }
        if (input.matches(ANY_DIGIT)) {
            throw new RuntimeException("Digits not allowed");
        }
    }

    private static void resetCounter(final Map<String, Integer> countByCharacter) {
        countByCharacter.replaceAll((k, v) -> 0);
    }

    // 1 and 2 length words will always match so skip them from loops
    private static int countGuaranteedResult(final String input) {
        final int oneLetterWords = input.length();
        final int twoLetterWords = input.length() - 1;
        return oneLetterWords + twoLetterWords;
    }

    private static boolean isBalanced(final Map<String, Integer> count, final String newCharactedToConsider) {
        int firsCount = count.get(newCharactedToConsider);
        for (String key : count.keySet()) {
            if (count.get(key) == 0 || key.equals(newCharactedToConsider)) {
                continue;
            }

            if (count.get(key) != firsCount) {
                return false;
            }
        }
        return true;
    }


    @Override
    public String name() {
        return "Maciek 2.0";
    }
}