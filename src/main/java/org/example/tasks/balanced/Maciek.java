package org.example.tasks.balanced;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Maciek implements BalancedWords{

    private static final String ANY_DIGIT = ".*\\d+.*";

    @Override
    public int countBalancedWords(final String input) {
        validateInput(input);
        if (input.length() == 0) {
            return 0;
        }

        int result = countGuaranteedResult(input);

        final List<String> byLetters = Arrays.asList(input.split(""));

        for (int wordSize = 3; wordSize <= byLetters.size(); wordSize++) {

            for (int startPoint = 0; startPoint < byLetters.size(); startPoint++) {
                int endPoint = startPoint + wordSize;
                if (endPoint > byLetters.size()) {
                    break;
                }
                final List<String> subWord = byLetters.subList(startPoint, endPoint);

                if (isBalanced(subWord)) {
                    result++;
                }
            }
        }

        return result;
    }

    private static void validateInput(final String input) {
        if (input == null) {
            throw new RuntimeException("Input is null");
        }
        if (input.matches(ANY_DIGIT)) {
            throw new RuntimeException("Digits not allowed");
        }
    }

    // 1 and 2 length words will always match so skip them from loops
    private static int countGuaranteedResult(final String input) {
        final int oneLetterWords = input.length();
        final int twoLetterWords = input.length() - 1;
        return oneLetterWords + twoLetterWords;
    }

    private static boolean isBalanced(final List<String> subWord) {
        final Map<String, Long> count = countCharOccurrence(subWord);
        final Set<Long> uniqueCounts = Set.copyOf(count.values());

        return uniqueCounts.size() == 1;
    }

    private static Map<String, Long> countCharOccurrence(final List<String> subWord) {
        return subWord.parallelStream()
                .collect(Collectors.groupingBy(
                        Function.identity(), Collectors.counting())
                );
    }

    @Override
    public String name() {
        return "Maciek 1.0";
    }
}