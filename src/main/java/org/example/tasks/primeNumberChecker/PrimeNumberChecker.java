package org.example.tasks.primeNumberChecker;

import java.util.ArrayList;
import java.util.List;

/*
 * A prime number is any whole number greater than 1 whose only factors are 1 and itself.
 * For example, 7 is a prime number because it’s only divisible by 1 and 7.
 * Create a function that returns TRUE if an input number is prime.
 *
 * The first few prime numbers are 2, 3, 5, 7, 11, 13, 17, and 19.
 * For a slightly harder challenge, create a prime number calculator that outputs all prime numbers between 2 and the input number.*/
public class PrimeNumberChecker {

    static boolean isPrimeNumber(int n) {
        if (n <= 1) {
            return false;
        }
        if (n == 2 || n == 3) {
            return true;
        }

        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }

    static List<Integer> getPrimeNumbers(int input) {
        List<Integer> primeNumbers = new ArrayList<>();

        for (int i = 0; i <= input; i++) {
            if (isPrimeNumber(i)) {
                primeNumbers.add(i);
            }
        }
        return primeNumbers;
    }

}
